<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr6-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Cache;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Psr\Cache\CacheItemInterface;
use Stringable;

/**
 * Psr6ToYii2CacheItem class file.
 * 
 * This class represents an item cached to be psr-6 compliant.
 * 
 * @author Anastaszor
 */
class Psr6ToYii2CacheItem implements CacheItemInterface, Stringable
{
	
	/**
	 * The key of the item.
	 * 
	 * @var string
	 */
	protected $_key;
	
	/**
	 * The expiration date.
	 * 
	 * @var DateTimeImmutable
	 */
	protected $_expires;
	
	/**
	 * The actual value.
	 * 
	 * @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	protected $_value;
	
	/**
	 * Builds a new CacheItem with its.
	 * 
	 * @param string $key
	 * @param integer $ttl
	 */
	public function __construct(string $key, int $ttl = 0)
	{
		$this->_key = $key;
		$this->_expires = new DateTimeImmutable();
		$this->expiresAfter($ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::getKey()
	 * @return string
	 */
	public function getKey()
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::get()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function get()
	{
		return $this->_value;
	}
	
	/**
	 * Gets the time to live of this item.
	 * 
	 * @return integer
	 */
	public function getDuration()
	{
		return \max(0, \time() - $this->_expires->getTimestamp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::isHit()
	 * @return boolean
	 */
	public function isHit()
	{
		return null !== $this->_value && $this->_expires->getTimestamp() < \time();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::set()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @return static
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function set($value)
	{
		$this->_value = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::expiresAt()
	 * @param ?DateTimeInterface $expiration
	 * @return static
	 */
	public function expiresAt($expiration)
	{
		if($expiration instanceof DateTimeInterface)
		{
			$this->_expires = new DateTimeImmutable('@'.((string) $expiration->getTimestamp()));
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Cache\CacheItemInterface::expiresAfter()
	 * @param null|int|DateInterval $time
	 * @return static
	 */
	public function expiresAfter($time)
	{
		if($time instanceof DateInterval)
		{
			$this->_expires = $this->_expires->add($time);
		}
		
		if(\is_int($time) && 0 < $time)
		{
			$this->_expires = $this->_expires->add(new DateInterval('P0DT'.((string) $time).'S'));
		}
		
		return $this;
	}
	
}
